#include <iostream>
using namespace std;

int main()
{
    int value1 = 3; //объявляю две целочисленные пер. и одну строку
    int value2 = 4; 
    string quote("Волк слабее льва и тигра, но в цирке он не выступает.");
    
    value1 = value1 * 3; //производим изменения значений
    value2 = value2 + 12;
    
    cout << value1 << " " << value2 << " " << quote; //вывод
}