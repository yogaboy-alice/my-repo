#include <iostream>
using namespace std;

int main()
{
    int value1 = 3; //объявление переменных
    int value2 = 4; 
    int value3 = 0;
    
    int buf = value1; //меняем местами через буферную переменную
    value1 = value2;
    value2 = buf;
    
    cout << "value1: " << value1 << "\n"; //вывод
    cout << "value2: " << value2 << "\n";
    cout << "value3: " << value3 << "\n";
}