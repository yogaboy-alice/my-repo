#include <iostream>
using namespace std;

int main()
{
    int milk_price = 62; //переменные цен
    int matches_price = 12;
    
    int milk_bought, matches_bought; //переменные количества
    cout << "1. Сколько куплено пакетов молока? \n 2. Сколько куплено коробков спичек? \n";
    cin >> milk_bought >> matches_bought; //ввод количества
    
    cout << "Общая стоимость - " << milk_bought * milk_price + matches_bought * matches_price; //подсчет и вывод
}